from typing import Optional

from fastapi import FastAPI
from fastapi.responses import HTMLResponse

app = FastAPI()

current_ip_pool = {}

@app.get("/")
def read_root():
	return current_ip_pool

@app.get("/addip/{poolname}/{ip}")
def store_ip_in_pool(poolname: str, ip: str):
	global current_ip_pool

	if poolname in current_ip_pool:
		current_ip_pool[ poolname].append(ip)
	else:
		current_ip_pool[ poolname ] = [ip]

	return {"poolname": poolname, 'IP' : ip}

@app.get("/pool/{poolname}")
def get_pool_ips(poolname: str):
	global current_ip_pool
	
	if poolname in current_ip_pool:
		return { 'ips' : current_ip_pool[poolname], 'message' : 'found' }
	else:
		return { 'ips' : [], 'message': 'not_found'}

	return { 'ips' : [], 'message': 'not_found'}

@app.get("/flatfile/{poolname}", response_class=HTMLResponse)
def generate_flatfile_for_poolname(poolname: str):
	global current_ip_pool
	
	flatfile = ""

	if poolname in current_ip_pool:
		for host in current_ip_pool[poolname]:
			flatfile += "{}:54322\n".format(host)

		return flatfile
	else:
		return "EMPTY"
	return "EMPTY"


@app.get("/reset/{poolname}")
def reset_pool(poolname: str):
	global current_ip_pool
	
	if poolname in current_ip_pool:
		current_ip_pool.pop(poolname, None)
		return { 'message' : 'deleted' }
	else:
		return { 'message': 'not_found'}

	return {'message': 'not_found'}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
	return {"item_id": item_id, "q": q}